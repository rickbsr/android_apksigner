# android_ApkSigner
Google： Android APK Tools

***
#### 項目 ####
Google Android APK Signer

#### 簡介 ####
Google 提供之 Android APK 簽章工具，可對 APK 檔案進行簽章。

#### 備註 ####
須要「platform.pk8」和「platform.x509.pem」兩把金鑰。

***